import React, { useState, useEffect } from 'react'
import classnames from 'classnames'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './Newsletter.scss'

const panelClosed = 'panelClosed'
const tenMinutes = 10 * (60 * 1000) // 10 * 1 minute

const Newsletter = () => {
    const [email, setEmail] = useState('')
    const [show, setShow] = useState(false)

    useEffect(() => {
        window.addEventListener('scroll', bodyScrollHandler)
        return () => {
            window.removeEventListener('scroll', bodyScrollHandler)
        }
    })

    const getClosedStatus = () => {
        let closedTime = localStorage.getItem(panelClosed)
        if(!closedTime) return false
        closedTime = new Date(closedTime).getTime()
        let now = new Date().getTime()
        if(now - closedTime < tenMinutes) return true
        localStorage.removeItem(panelClosed)
        return false
    }

    const bodyScrollHandler = () => {
        let nowClosed = getClosedStatus()
        let content = document.getElementById('content')
        let top = content.getBoundingClientRect().top
        let pageHeight = content.offsetHeight - window.innerHeight
        let boundary = (pageHeight/3) * -1
        if(top < boundary && !nowClosed) {
            setShow(true)
        }
    }

    const onCloseHandler = () => {
        setShow(false)
        localStorage.setItem(panelClosed, new Date())
    }

    const submitHandler = e => {
        e.preventDefault()
    }

    return (
        <div className={ classnames("newsletter", {
                "newsletter--show" : show
            })}
        >
            <div className="newsletter__wrapper">
                <button
                    onClick={onCloseHandler}
                    className="newsletter__wrapper__close"
                >
                    <FontAwesomeIcon icon="times" />
                </button>
                <h1>Get latest updates in web technologies</h1>
                <p>
                    I write articles related to web technologies, such as design trends, development
                    tools, UI/UX case studies and reviews, and more. Sign up to my newsletter to get
                    them all.
                </p>
                <form className="newsletter__wrapper__form" onSubmit={submitHandler}>
                    <input
                        onChange={e => setEmail(e.target.value)}
                        name="newletter_input"
                        id="newletter_input"
                        value={email}
                        placeholder="Email address"
                    />
                    <button type="submit">
                        Count me in!
                    </button>
                </form>
            </div>
        </div>
    )
}

export default Newsletter