import React from 'react'
import './Footer.scss'

const Footer = () => (
    <div className="footer">
        <div className="footer_wrapper">
            © 2018 Fauzi Al Aziz. All rights reserved.
        </div>
    </div>
)

export default Footer