import React from 'react'
import './HeroShot.scss'
import logo from '../../images/logo.png'

const HeroShot = () => (
    <div className="hero">
        <div className="hero__center">
            <img className="hero__center__logo" src={logo} alt="logo" />
        </div>
        <div className="hero__wrapper">
            <h1>Hello! I'm Fauzi Al Aziz</h1>
            <h2>Consult, Design, and Develop Websites</h2>
            <p>Have something great in mind? Feel free to contact me.</p>
            <p>I'll help you to make it happen.</p>
            <button>
                Let's Make Contact
            </button>
        </div>
    </div>
)

export default HeroShot