import React, { useState, useEffect, Fragment } from 'react'
import classnames from 'classnames'
import './Notification.scss'

const Notification = ({children}) => {
    const [show, setShow] = useState(true)
    const [heigth, setHeigth] = useState(0)

    useEffect( () => {
        window.addEventListener('resize', resizeHandler)
        let notif = document.getElementById('notif')
        setHeigth(notif.clientHeight)
        return () => {
            window.removeEventListener('resize', resizeHandler)
        }
    }, [])

    const resizeHandler = () => {
        let notif = document.getElementById('notif')
        setHeigth(notif.clientHeight)
    }

    return (
        <Fragment>
            <div className={
                classnames('notif', {
                    'notif--hide' : !show
                })}
                id="notif"
            >
                <div className="notif__wrapper">
                    <p>
                        By accessing and using this website, you acknowledge that you have read and
                        understand our <a href="/">Cookie Policy, Privacy Policy</a>, and our <a href="/">Terms of Service</a>.
                    </p>
                    <button
                        onClick={() => setShow(false)}
                        name='notif_button'
                        className="notif__wrapper__button"
                    >
                        Got It
                    </button>
                </div>
            </div>
            <div id="content" className="content" style={{
                paddingTop:show? `${heigth}px`: '0px'
            }}>
                {children}
            </div>
        </Fragment>
    )
}

export default Notification