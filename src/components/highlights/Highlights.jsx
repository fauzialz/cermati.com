import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './Highlights.scss'

const items = [
    {
        name: 'Consult',
        description: 'Co-create, design thinking; strengthen infrastructure resist granular. Revolution circular, movements or framework social impact low-hanging fruit. Save the world compelling revolutionary progress.',
        icon: 'comments'
    },
    {
        name: 'Design',
        description: 'Policymaker collaborates collective impact humanitarian shared value vocabulary inspire issue outcomes agile. Overcome injustice deep dive agile issue outcomes vibrant boots on the ground sustainable.',
        icon: 'paint-brush'
    },
    {
        name: 'Develop',
        description: 'Revolutionary circular, movements a or impact framework social impact low-hanging. Save the compelling revolutionary inspire progress. Collective impacts and challenges for opportunities of thought provoking.',
        icon: 'cubes'
    },
    {
        name: 'Marketing',
        description: 'Peaceful; vibrant paradigm, collaborative cities. Shared vocabulary agile, replicable, effective altruism youth. Mobilize commitment to overcome injustice resilient, uplift social transparent effective.',
        icon: 'bullhorn'
    },
    {
        name: 'Manage',
        description: 'Change-makers innovation or shared unit of analysis. Overcome injustice outcomes strategize vibrant boots on the ground sustainable. Optimism, effective altruism invest optimism corporate social.',
        icon: 'sliders-h'
    },
    {
        name: 'Evolve',
        description: 'Activate catalyze and impact contextualize humanitarian. Unit of analysis overcome injustice storytelling altruism. Thought leadership mass incarceration. Outcomes big data, fairness, social game-changer.',
        icon: 'chart-line'
    }
]

const Highlights = () => (
    <div className="highlights">
        <div className="highlights__center">
            <div className="highlights__header">
                <h1>How Can I Help You?</h1>
                <div className="highlights__header__text">
                    <p>
                        Our work then targeted, best practices outcomes social innovation synergy.
                        Venture philanthropy, revolutionary inclusive policymaker relief. User-centered
                        program areas scale.
                    </p>
                </div>
            </div>
            <div className="highlights__list">
                {items.map(item => (
                    <div className="highlights__list__item" key={item.name}>
                        <div className="highlights__list__item__wrapper">
                            <FontAwesomeIcon icon={item.icon} />
                            <div className="highlights__list__item__wrapper__title">{item.name}</div>
                            <div className="highlights__list__item__wrapper__text">{item.description}</div>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    </div>
)

export default Highlights