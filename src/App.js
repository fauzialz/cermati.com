import React from 'react';
import './App.css';
import Notification from './components/notification';
import HeroShot from './components/heroShot/HeroShot';
import Highlights from './components/highlights/Highlights';
import Footer from './components/footer';
import Newsletter from './components/newsletter';

function App() {
  return (
    <Notification>
      <HeroShot />
      <Highlights />
      <Footer />
      <Newsletter />
    </Notification>
  );
}

export default App;
